# Parallax using Translate3d

Use translate for parallax to utilize the user's hardware to make for smoother scrolling across browsers.
This also allows for transition effects and greater CSS styling abilities since it's not using a background image but instead using an img element.

#####Settings
| Option           | Type     | Default                     | Possible Values | Description                                                                        |   |
|------------------|----------|-----------------------------|-----------------|------------------------------------------------------------------------------------|---|
| image            | String   | /public/images/parallax.jpg |                 | Path to the image                                                                  |   |
| centerElement    | Boolean  | true                        | true/false      | Center the image if larger than window                                             |   |
| boundaries       | Boolean  | true                        | true/false      | Give the parallax container boundaries                                             |   |
| stayInBoundaries | Boolean  | true                        | true/false      | Stay in the boundaries of the parallax container                                   |   |
| scrollSpeed      | Number   | 0                           |                 | The amount the parallax scrolls (_boundaries: false_)                              |   |
| offset           | Number   | 0                           |                 | The offset of the image when parallax is centered on screen (_boundaries: false_)  |   |
| overflow         | String   | hidden                      | hidden/visible  | Control the overflow on the parent element                                         |   |
| direction        | Object   |                             |                 |                                                                                    |   |
| direction.move   | String   | y                           | x/y/xy          | Direction to move the parallax element                                             |   |
| direction.y      | String   | up                          | up/down         | Move up or down                                                                    |   |
| direction.x      | String   | ltr                         | ltr/rtl         | Move left-to-right(ltr) or right-to-left(rtl                                       |   |
| onLoad           | Function | null                        |                 | Function to run when the plugin is setup but before it runs                        |   |
| beforeMove       | Function | null                        |                 | Function to run **before** the image moves                                         |   |
| afterMove        | Function | null                        |                 | Function to run **after** the image moves                                          |   |
| step             | Object   |                             |                 |                                                                                    |   |
| step.forward     | Function | null                        |                 | Function to run on each movement of the parallax while scrolling **down** the page |   |
| step.backward    | Function | null                        |                 | Function to run on each movement of the parallax while scrolling **up** the page   |   |
| onResize         | Function | null                        |                 | Function to run when window resizes                                                |   |

#####HTML
Give the parent element the **parallax** class and set the attribute **data-parallax-image** with the value of where the image is located.
You can also set the image inside the initialization call, but if there are multiple parallax sections on the same page, you'll want to use
the **data-parallax-image** method so you can have different images for each section.

You can also change how the parallax function using html data attributes.

**HTML SETTINGS**

| Option                           | JS Equivalent                                                   |
|----------------------------------|-----------------------------------------------------------------|
| data-parallax-image              | image                                                           |
| data-parallax-center-element       | centerElement                                                     |
| data-parallax-boundaries         | boundaries                                                      |
| data-parallax-stay-in-boundaries | stayInBoundaries                                                |
| data-parallax-scroll-speed       | scrollSpeed                                                     |
| data-parallax-offset             | offset                                                          |
| data-parallax-overflow           | overflow                                                        |
| data-parallax-direction-move     | direction.move                                                  |
| data-parallax-direction-y        | direction.y                                                     |
| data-parallax-direction-x        | direction.x                                                     |
| data-parallax-instance           | N/A; Give that instance of parallax a name for easier debugging |


```html
<section class="parallax one" data-parallax-image="/public/images/parallax/parallax1.jpg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- HTML CONTENT -->
			</div>
		</div>
	</div>
</section>
```

#####CSS  
Grab the CSS from the "style.css" in the "css" folder
Adjust the padding on **.parallax .row** as necessary to give the best effect for your site


#####JavaScript
Initialize parallax
```javascript
<script>
	$(document).ready(function () {
		$('.parallax').parallax();
	});
</script>
```

######Complex Installation
For a more complex installation and to demonstrate the step functionality.
Use the **step.forward** and **step.backward** callbacks to change the image from black & white to colored when
the parallax has completed a certain percent.

```html
<section id="parallax-complex" class="parallax two flyin" data-parallax-instance="image-swap">
	<img class="parallax-element" src="/public/images/parallax/parallax1.jpg" />
	<div class="parallax-wrap">
		<img class="parallax-element swap-image" src="/public/images/parallax/parallax1.jpg" />
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12" >
				<!-- Content -->
			</div>
		</div>
	</div>
</section>
```
```css
.parallax .parallax-wrap {
	position: absolute;
	top: 0;
	overflow: hidden;
	width: 100%;
}
.parallax .parallax-element.swap-image {
	opacity: 0;
	transition: 1s opacity ease;
}
.parallax .parallax-element.swap-image.swapped {
	opacity: 1;
	transition: 1s opacity ease;
}

.parallax .parallax-element:not(.swap-image) {
	filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale");
	filter: gray; /* IE6-9 */
	filter: grayscale(100%);
	-webkit-filter: grayscale(100%);
	-moz-filter: grayscale(100%);
	-o-filter: grayscale(100%);
	-ms-filter: grayscale(100%);
}
```
```javascript
$('.parallax').parallax({
	step: {
		forward: function (e) {
			if (e.instance === 'image-swap') {
				if (e.percentComplete > 40 && !imageSwapped) {
					imageSwapped = true;
					$(e.el).find('.parallax-element').addClass('swapped');
					$(e.el).find('.parallax-element.swap-image').width(e.geo.parallax.element.width).height(e.geo.parallax.element.height).parent('.parallax-wrap').animate({
						height: e.geo.parallax.viewport.height + 'px'
					}, {
						duration: 2000
					});
				}
			}
		},
		backward: function (e) {
			if (e.instance === 'image-swap') {
				if (e.percentComplete < 30 && imageSwapped) {
					imageSwapped = false;
					$(e.el).find('.parallax-element').removeClass('swapped');
					$(e.el).find('.parallax-wrap').animate({
						height: '0px'
					}, {
						duration: 2000
					});
				}
			}
		}
	}
});
```